<?php

use Illuminate\Database\Eloquent\Model as Eloquent;

class Answer extends Eloquent {
  protected $table = "answer";
  public function question()
  {
    return $this->belongsTo('Question',"question_id");
  }
  
  public function category()
  {
    return $this->belongsToMany('Category',"scores")->withPivot('points');
  }
}
