<?php

use Illuminate\Database\Eloquent\Model as Eloquent;

class Category extends Eloquent {
  protected $table = "category";
  
  public function answers()
  {
    return $this->belongsToMany('Answer',"scores")->withPivot('points'); 
  }
}