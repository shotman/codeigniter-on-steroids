<?php

class Question extends Eloquent {
  protected $table = "question";
  public function answers()
  {
    return $this->hasMany('Answer');
  }
}